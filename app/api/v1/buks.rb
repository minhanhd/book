module API
  module V1
    class Buks < Grape::API
      include API::V1::ExceptionsHandler

      resource :buks do
        desc "Return all buks"
        get "", root: :buks do
          buks = Buk.all
          present buks, with: API::Entities::Buk
        end

        desc "Return a buk"
        params do
          requires :id, type: String, desc: "ID of the buk"
        end
        get ":id", root: "buk" do
          Buk.where(id: params[:id]).first
        end
      end
    end
  end
end
