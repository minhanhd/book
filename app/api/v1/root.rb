module API
  module V1
    class Root < Grape::API
      version 'v1'

      mount V1::Buks
      # mount API::V1::AnotherResource
    end
  end
end
