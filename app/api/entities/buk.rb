module API
  module Entities
    class Buk < API::Entities::Base
      root 'data', 'object'

      expose :id,
             :title,
             :author,
             :description,
             :title
    end
  end
end
