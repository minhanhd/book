json.extract! buk, :id, :title, :description, :image_url, :author, :created_at, :updated_at
json.url buk_url(buk, format: :json)
