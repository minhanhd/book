class BuksController < ApplicationController
  before_action :set_buk, only: [:show, :edit, :update, :destroy]

  # GET /buks
  # GET /buks.json
  def index
    @buks = Buk.all
  end

  # GET /buks/1
  # GET /buks/1.json
  def show
  end

  # GET /buks/new
  def new
    @buk = Buk.new
  end

  # GET /buks/1/edit
  def edit
  end

  # POST /buks
  # POST /buks.json
  def create
    @buk = Buk.new(buk_params)

    respond_to do |format|
      if @buk.save
        format.html { redirect_to @buk, notice: 'Buk was successfully created.' }
        format.json { render :show, status: :created, location: @buk }
      else
        format.html { render :new }
        format.json { render json: @buk.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /buks/1
  # PATCH/PUT /buks/1.json
  def update
    respond_to do |format|
      if @buk.update(buk_params)
        format.html { redirect_to @buk, notice: 'Buk was successfully updated.' }
        format.json { render :show, status: :ok, location: @buk }
      else
        format.html { render :edit }
        format.json { render json: @buk.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /buks/1
  # DELETE /buks/1.json
  def destroy
    @buk.destroy
    respond_to do |format|
      format.html { redirect_to buks_url, notice: 'Buk was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_buk
      @buk = Buk.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def buk_params
      params.require(:buk).permit(:title, :description, :image_url, :author)
    end
end
