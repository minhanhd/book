require "application_system_test_case"

class BuksTest < ApplicationSystemTestCase
  setup do
    @buk = buks(:one)
  end

  test "visiting the index" do
    visit buks_url
    assert_selector "h1", text: "Buks"
  end

  test "creating a Buk" do
    visit buks_url
    click_on "New Buk"

    fill_in "Author", with: @buk.author
    fill_in "Description", with: @buk.description
    fill_in "Image Url", with: @buk.image_url
    fill_in "Title", with: @buk.title
    click_on "Create Buk"

    assert_text "Buk was successfully created"
    click_on "Back"
  end

  test "updating a Buk" do
    visit buks_url
    click_on "Edit", match: :first

    fill_in "Author", with: @buk.author
    fill_in "Description", with: @buk.description
    fill_in "Image Url", with: @buk.image_url
    fill_in "Title", with: @buk.title
    click_on "Update Buk"

    assert_text "Buk was successfully updated"
    click_on "Back"
  end

  test "destroying a Buk" do
    visit buks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Buk was successfully destroyed"
  end
end
