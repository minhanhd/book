require 'test_helper'

class BuksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @buk = buks(:one)
  end

  test "should get index" do
    get buks_url
    assert_response :success
  end

  test "should get new" do
    get new_buk_url
    assert_response :success
  end

  test "should create buk" do
    assert_difference('Buk.count') do
      post buks_url, params: { buk: { author: @buk.author, description: @buk.description, image_url: @buk.image_url, title: @buk.title } }
    end

    assert_redirected_to buk_url(Buk.last)
  end

  test "should show buk" do
    get buk_url(@buk)
    assert_response :success
  end

  test "should get edit" do
    get edit_buk_url(@buk)
    assert_response :success
  end

  test "should update buk" do
    patch buk_url(@buk), params: { buk: { author: @buk.author, description: @buk.description, image_url: @buk.image_url, title: @buk.title } }
    assert_redirected_to buk_url(@buk)
  end

  test "should destroy buk" do
    assert_difference('Buk.count', -1) do
      delete buk_url(@buk)
    end

    assert_redirected_to buks_url
  end
end
