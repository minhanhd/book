class CreateBuks < ActiveRecord::Migration[5.2]
  def change
    create_table :buks do |t|
      t.string :title
      t.text :description
      t.string :image_url
      t.string :author

      t.timestamps
    end
  end
end
