Rails.application.routes.draw do
  root 'buks#index'
  resources :buks
  mount API::Root => '/'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
